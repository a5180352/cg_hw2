#version 330
layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 texcoord;

out VS_OUT {
    vec3 FragPos;
    vec3 Normal;
    vec2 TexCoords;
    vec4 FragPosLightSpace;
} vs_out;

uniform mat4 P;
uniform mat4 VM;
uniform mat4 M;
uniform mat4 lightProjection;
uniform mat4 lightView;

void main() {
    gl_Position = P * VM * vec4(position, 1.0f);
    vs_out.FragPos = vec3(M * vec4(position, 1.0));
    vs_out.Normal = transpose(inverse(mat3(M))) * normal;
    vs_out.TexCoords = texcoord;
    vs_out.FragPosLightSpace = lightProjection * lightView * vec4(vs_out.FragPos, 1.0);
}