#version 330
layout (location = 0) in vec3 position;
layout (location = 2) in vec2 texcoord;

out VS_OUT {
    vec2 TexCoords;
} vs_out;

uniform mat4 lightProjection;
uniform mat4 lightView;
uniform mat4 model;

void main() {
    gl_Position = lightProjection * lightView * model * vec4(position, 1.0f);
    vs_out.TexCoords = texcoord;
}