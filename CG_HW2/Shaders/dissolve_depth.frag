#version 330
in VS_OUT {
    vec2 TexCoords;
} fs_in;

uniform sampler2D noiseTexture;
uniform float threshold;

void main() {
    // gl_FragDepth = gl_FragCoord.z;
    float noise = texture(noiseTexture, fs_in.TexCoords).r;
	if (noise < threshold) {
		discard;
	}
}