/*

    CG Homework2 - Shadow Mapping & Dissolve Effects

    Objective - learning Shadow Implmentation and Dissolve Effects

    Overview:

        1. Render the model with Shadow using shadow mapping

        2. Implement dissolve effect

    !!!IMPORTANT!!!

    1. Make sure to change the window name to your student ID!
    2. You are allow to use glmDraw this time.

*/

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h> // for function: offsetof
#include <math.h>
#include <string.h>
#include "../GL/glew.h"
#include "../GL/glut.h"
#include "../shader_lib/shader.h"
#include "glm/glm.h"
extern "C" {
    #include "glm_helper.h"
}
#include <string>
#include <vector>
#include <iostream>

// you may need to do something here
GLuint depthMapFBO;
GLuint depthMap;
GLuint VAO[3], VBO[3];
const GLuint IDX_MODEL = 0, IDX_PLANE = 1, IDX_SUB = 2;
GLfloat lightProjection[16], lightView[16];
GLfloat mat4_model[3][16], mat4_project[3][16], mat4_view[3][16];

/*
    you may use the following struct type to perform your single VBO method,
    or you can define/declare multiple VBOs for VAO method.
    Please feel free to modify it
*/
struct Vertex {
    GLfloat position[3];
    GLfloat normal[3];
    GLfloat texcoord[2];
};
typedef struct Vertex Vertex;
std::vector<Vertex> vertex_list[3];

void renderDepthMap(GLuint model_index, bool is_dissolve);
void drawModel(GLuint model_index, GLuint main_tex_id, bool is_dissolve);

// no need to modify the following function declarations and gloabal variables
void init(void);
void display(void);
void reshape(int width, int height);
void keyboard(unsigned char key, int x, int y);
void keyboardup(unsigned char key, int x, int y);
void motion(int x, int y);
void mouse(int button, int state, int x, int y);
void idle(void);
void draw_light_bulb(void);
void camera_light_ball_move();
GLuint loadTexture(char* name, GLfloat width, GLfloat height);

namespace {
    char *obj_file_dir   = "../Resources/Ball.obj";
    char *obj_file_dir2  = "../Resources/bunny.obj";
    char *main_tex_dir   = "../Resources/Stone.ppm";
    char *floor_tex_dir  = "../Resources/WoodFine.ppm";
    char *plane_file_dir = "../Resources/Plane.obj";
    char *noise_tex_dir  = "../Resources/noise.ppm";

    GLfloat light_rad = 0.05; // radius of the light bulb
    float eyet = -5.59; // theta in degree
    float eyep = 83.2;  // phi in degree
    bool mleft     = false;
    bool mright    = false;
    bool mmiddle   = false;
    bool forward   = false;
    bool backward  = false;
    bool left      = false;
    bool right     = false;
    bool up        = false;
    bool down      = false;
    bool lforward  = false;
    bool lbackward = false;
    bool lleft     = false;
    bool lright    = false;
    bool lup       = false;
    bool ldown     = false;
    bool bforward  = false;
    bool bbackward = false;
    bool bleft     = false;
    bool bright    = false;
    bool bup       = false;
    bool bdown     = false;
    bool bx        = false;
    bool by        = false;
    bool bz        = false;
    bool brx       = false;
    bool bry       = false;
    bool brz       = false;

    int mousex = 0;
    int mousey = 0;
}

// You can modify the moving/rotating speed if it's too fast/slow for you
const float speed = 0.03;          // camera / light / ball moving speed
const float rotation_speed = 0.05; // ball rotating speed

// you may need to use some of the following variables in your program
const GLuint prog_count = 4;
GLuint programs[prog_count];
const GLuint IDX_SHADER = 0, IDX_DEPTH = 1, IDX_DIS = 2, IDX_DIS_DEPTH = 3;
const GLuint SHADOW_WIDTH = 512, SHADOW_HEIGHT = 512;
GLfloat threshold;

// No need for model texture, 'cause glmModel() has already loaded it for you.
// To use the texture, check glmModel documentation.
GLuint mainTextureID; // TA has already loaded this texture for you
GLuint floorTextureID;
GLuint noiseTextureID;

GLMmodel *model; // TA has already loaded the model for you(!but you still need to convert it to VBO(s)!)
GLMmodel *planeModel;
GLMmodel *subModel;

float eyex = -3.291;
float eyey = 1.57;
float eyez = 11.89;

GLfloat light_pos[]    = {   1.1,  3.5,  1.3};
GLfloat ball_pos[]     = {   0.0,  0.0,  0.0};
GLfloat ball_rot[]     = {   0.0,  0.0,  0.0};
GLfloat plane_pos[]    = {   0.0, -5.0,  0.0};
GLfloat plane_rot[]    = {   0.0,  0.0,  0.0};
GLfloat subModel_pos[] = {-2.295, -5.0, -2.0};
GLfloat subModel_rot[] = {   0.0,  0.0,  0.0};

int main(int argc, char *argv[]) {
    glutInit(&argc, argv);

    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);

    // remember to replace "YourStudentID" with your own student ID
    glutCreateWindow("CG_HW2_0856025");
    glutReshapeWindow(512, 512);

    glewInit();

    init();

    glutReshapeFunc(reshape);
    glutDisplayFunc(display);
    glutIdleFunc(idle);
    glutKeyboardFunc(keyboard);
    glutKeyboardUpFunc(keyboardup);
    glutMouseFunc(mouse);
    glutMotionFunc(motion);

    glutMainLoop();

    glmDelete(model);
    return 0;
}

void init(void) {
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glEnable(GL_CULL_FACE);

    mainTextureID  = loadTexture(main_tex_dir, 1024, 1024);
    floorTextureID = loadTexture(floor_tex_dir, 512,  512);
    noiseTextureID = loadTexture(noise_tex_dir, 320,  320);

    threshold = 0.0f;

    model = glmReadOBJ(obj_file_dir);
    glmUnitize(model);
    glmFacetNormals(model);
    glmVertexNormals(model, 90.0, GL_FALSE);
    glEnable(GL_DEPTH_TEST);
    print_model_info(model);

    planeModel = glmReadOBJ(plane_file_dir);
    glmFacetNormals(planeModel);
    glmVertexNormals(planeModel, 90.0, GL_FALSE);
    glEnable(GL_DEPTH_TEST);
    print_model_info(planeModel);

    subModel = glmReadOBJ(obj_file_dir2);
    glmFacetNormals(subModel);
    glmVertexNormals(subModel, 90.0, GL_FALSE);
    glEnable(GL_DEPTH_TEST);
    print_model_info(subModel);

    // you may need to do something here(create shaders/program(s) and create vbo(s)/vao from GLMmodel model)

    // APIs for creating shaders and creating shader programs have been done by TAs
    // following is an example for creating a shader program using given vertex shader and fragment shader
    std::string shader_name[prog_count] = {"shadow_mapping", "shadow_mapping_depth", "dissolve", "dissolve_depth"};
    for (int i = 0; i < prog_count; i++) {
        GLuint vert = createShader(("Shaders/" + shader_name[i] + ".vert").c_str(), "vertex");
        GLuint frag = createShader(("Shaders/" + shader_name[i] + ".frag").c_str(), "fragment");
        programs[i] = createProgram(vert, frag);
    }

    // Configure depth map FBO
    glGenFramebuffers(1, &depthMapFBO);
    // Create depth texture
    glGenTextures(1, &depthMap);
    glBindTexture(GL_TEXTURE_2D, depthMap);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, SHADOW_WIDTH, SHADOW_HEIGHT, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
        GLfloat borderColor[] = {1.0, 1.0, 1.0, 1.0};
        glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);

        glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
            glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthMap, 0);
            glDrawBuffer(GL_NONE);
            glReadBuffer(GL_NONE);
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glBindTexture(GL_TEXTURE_2D, 0);

    GLMmodel *model_ptr[3] = {model, planeModel, subModel};
    // get vertice from GLM
    for (int k = 0; k < 3; k++) {
        GLMgroup *tri_group = &model_ptr[k]->groups[0];
        for (GLuint i = 0; i < tri_group->numtriangles; i++) {
            GLMtriangle triangle = model_ptr[k]->triangles[tri_group->triangles[i]];
            for (GLuint j = 0; j < 3; j++) {
                Vertex vertex;
                memcpy(vertex.position, &model_ptr[k]->vertices[triangle.vindices[j] * 3],  sizeof(GLfloat) * 3);
                memcpy(vertex.normal,   &model_ptr[k]->normals[triangle.nindices[j] * 3],   sizeof(GLfloat) * 3);
                memcpy(vertex.texcoord, &model_ptr[k]->texcoords[triangle.tindices[j] * 2], sizeof(GLfloat) * 2);
                vertex_list[k].push_back(vertex);
            }
        }
        glGenVertexArrays(1, &VAO[k]);
        glGenBuffers(1, &VBO[k]);

        glBindVertexArray(VAO[k]);
            glBindBuffer(GL_ARRAY_BUFFER, VBO[k]);
                glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * vertex_list[k].size(), vertex_list[k].data(), GL_STATIC_DRAW);
                glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);
                glEnableVertexAttribArray(0);
                glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid *)offsetof(Vertex, normal));
                glEnableVertexAttribArray(1);
                glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid *)offsetof(Vertex, texcoord));
                glEnableVertexAttribArray(2);
        glBindVertexArray(0);
    }
}

void display(void)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // you may need to do something here(declare some local variables you need and maybe load Model matrix here...)

    // load info from glm
    glPushMatrix();
        glLoadIdentity();
        glTranslatef(ball_pos[0], ball_pos[1], ball_pos[2]);
        glRotatef(ball_rot[0], 1, 0, 0);
        glRotatef(ball_rot[1], 0, 1, 0);
        glRotatef(ball_rot[2], 0, 0, 1);
        glGetFloatv(GL_MODELVIEW_MATRIX, mat4_model[IDX_MODEL]);
    glPopMatrix();

    glPushMatrix();
        glLoadIdentity();
        glTranslatef(plane_pos[0], plane_pos[1], plane_pos[2]);
        glRotatef(plane_rot[0], 1, 0, 0);
        glRotatef(plane_rot[1], 0, 1, 0);
        glRotatef(plane_rot[2], 0, 0, 1);
        glGetFloatv(GL_MODELVIEW_MATRIX, mat4_model[IDX_PLANE]);
    glPopMatrix();

    glPushMatrix();
        glLoadIdentity();
        glTranslatef(subModel_pos[0], subModel_pos[1], subModel_pos[2]);
        glRotatef(subModel_rot[0], 1, 0, 0);
        glRotatef(subModel_rot[1], 0, 1, 0);
        glRotatef(subModel_rot[2], 0, 0, 1);
        glGetFloatv(GL_MODELVIEW_MATRIX, mat4_model[IDX_SUB]);
    glPopMatrix();

    // Render depth of scene to texture (from light's perspective)
    // - Get light projection/view matrix.
    GLfloat line = 15.0f, near_plane = -15.0f, far_plane = 100.0f;
    glMatrixMode(GL_PROJECTION);
        glPushMatrix();
            glLoadIdentity();
            glOrtho(-line, line, -line, line, near_plane, far_plane);
            glGetFloatv(GL_PROJECTION_MATRIX, lightProjection);
        glPopMatrix();
    glMatrixMode(GL_MODELVIEW);

    glPushMatrix();
        glLoadIdentity();
        gluLookAt(light_pos[0], light_pos[1], light_pos[2], 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);
        glGetFloatv(GL_MODELVIEW_MATRIX, lightView);
    glPopMatrix();
    // - render scene from light's point of view
    glPushMatrix();
        glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
            glClear(GL_DEPTH_BUFFER_BIT);
            glEnable(GL_DEPTH_TEST);
            // render plane
            renderDepthMap(IDX_PLANE, false);
            glClear(GL_DEPTH_BUFFER_BIT);
            // render ball
            renderDepthMap(IDX_MODEL, true);
            // render bunny
            renderDepthMap(IDX_SUB, false);
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glPopMatrix();

    // please try not to modify the following block of code(you can but you are not supposed to)
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(
        eyex,
        eyey,
        eyez,
        eyex + cos(eyet * M_PI / 180) * cos(eyep * M_PI / 180),
        eyey + sin(eyet * M_PI / 180),
        eyez - cos(eyet * M_PI / 180) * sin(eyep * M_PI / 180),
        0.0,
        1.0,
        0.0);

    glPushMatrix();
        glColor3f(1, 1, 1);
        draw_light_bulb();
    glPopMatrix();

    glPushMatrix();
        glTranslatef(plane_pos[0], plane_pos[1], plane_pos[2]);
        glRotatef(plane_rot[0], 1, 0, 0);
        glRotatef(plane_rot[1], 0, 1, 0);
        glRotatef(plane_rot[2], 0, 0, 1);
        glColor3f(1, 1, 1);
        // you may need to do something here(pass uniform variable(s) to shader and render the model)
        drawModel(IDX_PLANE, floorTextureID, false);
        // glmDraw(planeModel, GLM_TEXTURE);
    glPopMatrix();

    glPushMatrix();
        glTranslatef(ball_pos[0], ball_pos[1], ball_pos[2]);
        glRotatef(ball_rot[0], 1, 0, 0);
        glRotatef(ball_rot[1], 0, 1, 0);
        glRotatef(ball_rot[2], 0, 0, 1);
        glColor3f(1, 1, 1);
        // you may need to do something here(pass uniform variable(s) to shader and render the model)
        drawModel(IDX_MODEL, mainTextureID, true);
        // glmDraw(model, GLM_TEXTURE);
    glPopMatrix();

    glPushMatrix();
        glTranslatef(subModel_pos[0], subModel_pos[1], subModel_pos[2]);
        glRotatef(subModel_rot[0], 1, 0, 0);
        glRotatef(subModel_rot[1], 0, 1, 0);
        glRotatef(subModel_rot[2], 0, 0, 1);
        // glBindTexture(GL_TEXTURE0, mainTextureID);
        // you may need to do something here(pass uniform variable(s) to shader and render the model)
        drawModel(IDX_SUB, mainTextureID, false);
        // glmDraw(subModel, GLM_TEXTURE);
    glPopMatrix();

    glutSwapBuffers();
    camera_light_ball_move();
}

void renderDepthMap(GLuint model_index, bool is_dissolve) {
    GLuint depth_prog = (is_dissolve)? programs[IDX_DIS_DEPTH] : programs[IDX_DEPTH];
    glPushMatrix();
        glUseProgram(depth_prog);
            glBindVertexArray(VAO[model_index]);
                // render model
                glUniformMatrix4fv(glGetUniformLocation(depth_prog, "lightProjection"), 1, GL_FALSE, lightProjection);
                glUniformMatrix4fv(glGetUniformLocation(depth_prog, "lightView"),       1, GL_FALSE, lightView);
                glUniformMatrix4fv(glGetUniformLocation(depth_prog, "model"), 1, GL_FALSE, mat4_model[model_index]);
                if (is_dissolve) {
                    glUniform1f(glGetUniformLocation(depth_prog, "threshold"), threshold);
                    glActiveTexture(GL_TEXTURE2);
                    glBindTexture(GL_TEXTURE_2D, noiseTextureID);
                    glUniform1i(glGetUniformLocation(depth_prog, "noiseTexture"), 2);
                }
                glDrawArrays(GL_TRIANGLES, 0, vertex_list[model_index].size());
                if (is_dissolve) {
                    glBindTexture(GL_TEXTURE_2D, 0);
                }
            glBindVertexArray(0);
        glUseProgram(0);
    glPopMatrix();
}

void drawModel(GLuint model_index, GLuint main_tex_id, bool is_dissolve) {
    GLuint shader_prog = (is_dissolve)? programs[IDX_DIS] : programs[IDX_SHADER];
    glUseProgram(shader_prog);
        glBindVertexArray(VAO[model_index]);
            // pass light info
            glUniformMatrix4fv(glGetUniformLocation(shader_prog, "lightProjection"), 1, GL_FALSE, lightProjection);
            glUniformMatrix4fv(glGetUniformLocation(shader_prog, "lightView"),       1, GL_FALSE, lightView);
            // Pass value M, P, M*V
            glUniformMatrix4fv(glGetUniformLocation(shader_prog, "M"),  1, GL_FALSE, mat4_model[model_index]);
            glGetFloatv(GL_PROJECTION_MATRIX, mat4_project[model_index]);
            glUniformMatrix4fv(glGetUniformLocation(shader_prog, "P"),  1, GL_FALSE, mat4_project[model_index]);
            glGetFloatv(GL_MODELVIEW_MATRIX,  mat4_view[model_index]);
            glUniformMatrix4fv(glGetUniformLocation(shader_prog, "VM"), 1, GL_FALSE, mat4_view[model_index]);
            // diffuse
            glUniform3fv(glGetUniformLocation(shader_prog, "lightPos"), 1, light_pos);
            // specular
            glUniform3f(glGetUniformLocation(shader_prog, "viewPos"), eyex, eyey, eyez);
            // pass texture order
            glEnable(GL_TEXTURE_2D);
            glActiveTexture(GL_TEXTURE0);
            glBindTexture(GL_TEXTURE_2D, main_tex_id);
            glUniform1i(glGetUniformLocation(shader_prog, "diffuseTexture"), 0);
            glActiveTexture(GL_TEXTURE1);
            glBindTexture(GL_TEXTURE_2D, depthMap);
            glUniform1i(glGetUniformLocation(shader_prog, "shadowMap"), 1);
            if (is_dissolve) {
                glActiveTexture(GL_TEXTURE2);
                glBindTexture(GL_TEXTURE_2D, noiseTextureID);
                glUniform1i(glGetUniformLocation(shader_prog, "noiseTexture"), 2);
                glUniform1f(glGetUniformLocation(shader_prog, "threshold"), threshold);
                glEnable(GL_BLEND);
                glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
            }
            // Draw vertice
            glDrawArrays(GL_TRIANGLES, 0, vertex_list[model_index].size());
            glBindTexture(GL_TEXTURE_2D, 0);
        glBindVertexArray(0);
    glUseProgram(0);
}

// please implement mode increase/decrease dissolve threshold in case '-' and case '=' (lowercase)
void keyboard(unsigned char key, int x, int y) {
    switch (key) {
        case 27: {
            // ESC
            exit(0);
        }
        case '-': { 
            // you may need to do somting here
            if (threshold < 1.0f) {
                // increase dissolve threshold
                threshold += 0.05f;
            }
            std::cout << threshold << std::endl;
            break;
        }
        case '=': {
            // you may need to do somting here
            if (threshold > 0.0f) {
                // decrease dissolve threshold
                threshold -= 0.05f;
            }
            std::cout << threshold << std::endl;
            break;
        }
        case 'd': {
            right = true;
            break;
        }
        case 'a': {
            left = true;
            break;
        }
        case 'w': {
            forward = true;
            break;
        }
        case 's': {
            backward = true;
            break;
        }
        case 'q': {
            up = true;
            break;
        }
        case 'e': {
            down = true;
            break;
        }
        case 't': {
            lforward = true;
            break;
        }
        case 'g': {
            lbackward = true;
            break;
        }
        case 'h': {
            lright = true;
            break;
        }
        case 'f': {
            lleft = true;
            break;
        }
        case 'r': {
            lup = true;
            break;
        }
        case 'y': {
            ldown = true;
            break;
        }
        case 'i': {
            bforward = true;
            break;
        }
        case 'k': {
            bbackward = true;
            break;
        }
        case 'l': {
            bright = true;
            break;
        }
        case 'j': {
            bleft = true;
            break;
        }
        case 'u': {
            bup = true;
            break;
        }
        case 'o': {
            bdown = true;
            break;
        }
        case '7': {
            bx = true;
            break;
        }
        case '8': {
            by = true;
            break;
        }
        case '9': {
            bz = true;
            break;
        }
        case '4': {
            brx = true;
            break;
        }
        case '5': {
            bry = true;
            break;
        }
        case '6': {
            brz = true;
            break;
        }
        // special function key
        case 'z': {
            // move light source to front of camera
            light_pos[0] = eyex + cos(eyet * M_PI / 180) * cos(eyep * M_PI / 180);
            light_pos[1] = eyey + sin(eyet * M_PI / 180);
            light_pos[2] = eyez - cos(eyet * M_PI / 180) * sin(eyep * M_PI / 180);
            break;
        }
        case 'x': {
            // move ball to front of camera
            ball_pos[0] = eyex + cos(eyet * M_PI / 180) * cos(eyep * M_PI / 180) * 3;
            ball_pos[1] = eyey + sin(eyet * M_PI / 180) * 5;
            ball_pos[2] = eyez - cos(eyet * M_PI / 180) * sin(eyep * M_PI / 180) * 3;
            break;
        }
        case 'c': {
            // reset all pose
            light_pos[0] = 1.1;
            light_pos[1] = 3.5;
            light_pos[2] = 1.3;
            ball_pos[0] = 0;
            ball_pos[1] = 0;
            ball_pos[2] = 0;
            ball_rot[0] = 0;
            ball_rot[1] = 0;
            ball_rot[2] = 0;
            eyex = -3.291;
            eyey = 1.57;
            eyez = 11.89;
            eyet = -5.59; // theta in degree
            eyep = 83.2;  // phi in degree
            break;
        }
        default: {
            break;
        }
    }
}

// no need to modify the following functions
void reshape(int width, int height) {
    glViewport(0, 0, width, height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.001f, 100.0f);
    glMatrixMode(GL_MODELVIEW);
}

void motion(int x, int y) {
    if (mleft)
    {
        eyep -= (x-mousex) * 0.1;
        eyet -= (y - mousey) * 0.12;
        if (eyet > 89.9)
            eyet = 89.9;
        else if (eyet < -89.9)
            eyet = -89.9;
        if (eyep > 360)
            eyep -= 360;
        else if (eyep < 0)
            eyep += 360;
    }
    mousex = x;
    mousey = y;
}

void mouse(int button, int state, int x, int y) {
    if (button == GLUT_LEFT_BUTTON) {
        if (state == GLUT_DOWN && !mright && !mmiddle) {
            mleft = true;
            mousex = x;
            mousey = y;
        } else
            mleft = false;
    } else if (button == GLUT_RIGHT_BUTTON) {
        if (state == GLUT_DOWN && !mleft && !mmiddle) {
            mright = true;
            mousex = x;
            mousey = y;
        } else
            mright = false;
    } else if (button == GLUT_MIDDLE_BUTTON) {
        if (state == GLUT_DOWN && !mleft && !mright) {
            mmiddle = true;
            mousex = x;
            mousey = y;
        } else
            mmiddle = false;
    }
}

void camera_light_ball_move() {
    GLfloat dx = 0, dy = 0, dz = 0;
    if (left|| right || forward || backward || up || down) {
        if (left)
            dx = -speed;
        else if (right)
            dx = speed;
        if (forward)
            dy = speed;
        else if (backward)
            dy = -speed;
        eyex += dy *   cos(eyet * M_PI / 180) *cos(eyep * M_PI / 180)  + dx * sin(eyep * M_PI / 180);
        eyey += dy *   sin(eyet * M_PI / 180);
        eyez += dy * (-cos(eyet * M_PI / 180) *sin(eyep * M_PI / 180)) + dx * cos(eyep * M_PI / 180);
        if (up)
            eyey += speed;
        else if (down)
            eyey -= speed;
    }
    if (lleft || lright || lforward || lbackward || lup || ldown) {
        dx = 0;
        dy = 0;
        if (lleft)
            dx = -speed;
        else if (lright)
            dx = speed;
        if (lforward)
            dy = speed;
        else if (lbackward)
            dy = -speed;
        light_pos[0] += dy *   cos(eyet * M_PI / 180) * cos(eyep * M_PI / 180)  + dx * sin(eyep * M_PI / 180);
        light_pos[1] += dy *   sin(eyet * M_PI / 180);
        light_pos[2] += dy * (-cos(eyet * M_PI / 180) * sin(eyep * M_PI / 180)) + dx * cos(eyep * M_PI / 180);
        if (lup)
            light_pos[1] += speed;
        else if (ldown)
            light_pos[1] -= speed;
    }
    if (bleft || bright || bforward || bbackward || bup || bdown) {
        dx = 0;
        dy = 0;
        if (bleft)
            dx = -speed;
        else if (bright)
            dx = speed;
        if (bforward)
            dy = speed;
        else if (bbackward)
            dy = -speed;
        ball_pos[0] += dy *   cos(eyet * M_PI / 180) * cos(eyep * M_PI / 180)  + dx * sin(eyep * M_PI / 180);
        ball_pos[1] += dy *   sin(eyet * M_PI / 180);
        ball_pos[2] += dy * (-cos(eyet * M_PI / 180) * sin(eyep * M_PI / 180)) + dx * cos(eyep * M_PI / 180);
        if (bup)
            ball_pos[1] += speed;
        else if (bdown)
            ball_pos[1] -= speed;
    }
    if (bx || by || bz || brx || bry || brz) {
        dx = 0;
        dy = 0;
        dz = 0;
        if (bx)
            dx = -rotation_speed;
        else if (brx)
            dx = rotation_speed;
        if (by)
            dy = rotation_speed;
        else if (bry)
            dy = -rotation_speed;
        if (bz)
            dz = rotation_speed;
        else if (brz)
            dz = -rotation_speed;
        ball_rot[0] += dx;
        ball_rot[1] += dy;
        ball_rot[2] += dz;
    }
}

void draw_light_bulb() {
    GLUquadric *quad;
    quad = gluNewQuadric();
    glPushMatrix();
    glColor3f(0.4, 0.5, 0);
    glTranslatef(light_pos[0], light_pos[1], light_pos[2]);
    gluSphere(quad, light_rad, 40, 20);
    glPopMatrix();
}

void keyboardup(unsigned char key, int x, int y) {
    switch (key) {
        case 'd': {
            right =false;
            break;
        }
        case 'a': {
            left = false;
            break;
        }
        case 'w': {
            forward = false;
            break;
        }
        case 's': {
            backward = false;
            break;
        }
        case 'q': {
            up = false;
            break;
        }
        case 'e': {
            down = false;
            break;
        }
        case 't': {
            lforward = false;
            break;
        }
        case 'g': {
            lbackward = false;
            break;
        }
        case 'h': {
            lright = false;
            break;
        }
        case 'f': {
            lleft = false;
            break;
        }
        case 'r': {
            lup = false;
            break;
        }
        case 'y': {
            ldown = false;
            break;
        }
        case 'i': {
            bforward = false;
            break;
        }
        case 'k': {
            bbackward = false;
            break;
        }
        case 'l': {
            bright = false;
            break;
        }
        case 'j': {
            bleft = false;
            break;
        }
        case 'u': {
            bup = false;
            break;
        }
        case 'o': {
            bdown = false;
            break;
        }
        case '7': {
            bx = false;
            break;
        }
        case '8': {
            by = false;
            break;
        }
        case '9': {
            bz = false;
            break;
        }
        case '4': {
            brx = false;
            break;
        }
        case '5': {
            bry = false;
            break;
        }
        case '6': {
            brz = false;
            break;
        }
        default: {
            break;
        }
    }
}

void idle(void) {
    subModel_rot[1] += 1;
    glutPostRedisplay();
}

GLuint loadTexture(char* name, GLfloat width, GLfloat height) {
    return glmLoadTexture(name, false, true, true, true, &width, &height);
}
